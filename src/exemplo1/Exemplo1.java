/**
 * Exemplo1: Programacao com threads
 * Autor: Reinaldo Fernandes
 * Ultima modificacao: 07/08/2017
 */
package exemplo1;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Reinaldo Fernandes
 */
public class Exemplo1  {

        public static void main(String [] args){
            final int numThreads= 20;
            
            // cria o pool de threads
            ExecutorService obj = Executors.newCachedThreadPool();
            
            System.out.println("Inicio da criacao das threads.");
            
            // Crie 20 threads
            Thread t;
            for (int i=0; i<numThreads; i++){
               t = new Thread(new PrintTasks("thread"));
               obj.execute(t);
            }
            
            // nao aceita novas submissoes de threads
            obj.shutdown();
            
            System.out.println("Threads criadas");
        }
        
}



