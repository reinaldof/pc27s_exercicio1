/**
 * Exemplo1: Programacao com threads
 * Autor: Reinaldo Fernandes
 * Ultima modificacao: 07/08/2017
 */
package exemplo1;

import java.util.Random;

/**
 *
 * @author Reinaldo Fernandes
 */
public class PrintTasks implements Runnable {
    private static int KEY=0; // Crie no construtor da classe um identificador único para cada thread.
    private int ID;
    private final int sleepTime; //tempo de adormecimento aleatorio para a thread
    private final String taskName; //nome da tarefa
    private final static Random generator = new Random();

    public PrintTasks(String name){
        KEY ++;
        this.ID = KEY;
        taskName = name+" id "+ID;
        
        sleepTime = generator.nextInt(1000); //milissegundos
        
    }
    
    public void run(){
        try{
            //System.out.printf("Tarefa: %s -> %d ms\n", taskName, this.ID);
           if (this.ID%2==1){
                System.out.printf("Tarefa: %s dorme por %d ms\n", taskName, sleepTime);
                //Estado de ESPERA SINCRONIZADA
                //Nesse ponto, a thread perde o processador, e permite que
                //outra thread execute
                Thread.sleep(sleepTime);
           }
            
        } catch (InterruptedException ex){
            System.out.printf("%s %s\n", taskName, "terminou de maneira inesperada.");
        }
       System.out.printf("%s acordou!\n", taskName);
    }
       
}
